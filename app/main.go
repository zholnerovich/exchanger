package main

import (
	"exchanger/internal/handlers"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/robfig/cron/v3"
	"log"
	"net/http"
)

func main() {
	c := cron.New()
	// every minute
	_, err := c.AddFunc("* * * * *", handlers.FetchExchangeRatesHandle)
	if err != nil {
		fmt.Println(err)
		return
	}
	c.Start()

	r := mux.NewRouter()
	r.HandleFunc("/exchange-rates", handlers.GetAllExchangeRatesHandle)
	r.HandleFunc("/exchange-rates/{date:[0-9]{4}-[0-9]{2}-[0-9]{2}}", handlers.GetExchangeRatesByDateHandle)
	log.Fatal(http.ListenAndServe(":8080", r))
}
