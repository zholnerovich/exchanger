package entities

import (
	"encoding/json"
	"fmt"
)

type ExchangeRates struct {
	Date  string `json:"date"`
	Rates string `json:"-"`
	Data  any    `json:"data"`
}

func (e *ExchangeRates) DecodeRates() {
	if err := json.Unmarshal([]byte(e.Rates), &e.Data); err != nil {
		fmt.Println(err)
	}
}
