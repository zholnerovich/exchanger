package repositories

import (
	"database/sql"
	"exchanger/internal/entities"
)

type ExchangeRatesRepository struct{}

func (e *ExchangeRatesRepository) GetAll(db *sql.DB) (*[]entities.ExchangeRates, error) {
	var ratesArray []entities.ExchangeRates
	query := `SELECT date, rates FROM exchange_rates`
	rows, err := db.Query(query)

	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var rates entities.ExchangeRates
		err := rows.Scan(&rates.Date, &rates.Rates)
		if err != nil {
			return nil, err
		}
		rates.DecodeRates()
		ratesArray = append(ratesArray, rates)
	}
	return &ratesArray, nil
}

func (e *ExchangeRatesRepository) GetByDate(db *sql.DB, date string) (*entities.ExchangeRates, error) {
	var rates entities.ExchangeRates
	query := `SELECT date, rates FROM exchange_rates WHERE date = ?`
	row := db.QueryRow(query, date)
	err := row.Scan(&rates.Date, &rates.Rates)
	if err != nil {
		return nil, err
	}
	rates.DecodeRates()
	return &rates, nil
}

func (e *ExchangeRatesRepository) Store(db *sql.DB, er *entities.ExchangeRates) error {
	query := `INSERT INTO exchange_rates (date, rates) VALUES (?, ?)`
	_, err := db.Exec(query, er.Date, er.Rates)

	if err != nil {
		return err
	}
	return nil
}
