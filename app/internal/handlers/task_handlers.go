package handlers

import (
	"exchanger/internal/repositories"
	"exchanger/internal/services"
	"fmt"
)

func FetchExchangeRatesHandle() {
	one, err := new(services.ExchangeRatesApiService).FetchExchangeRates()
	if err != nil {
		fmt.Println(err)
		return
	}

	db, err := new(services.DBService).GetDB()
	if err != nil {
		fmt.Println(err)
		return
	}

	err = new(repositories.ExchangeRatesRepository).Store(db, one)
}
