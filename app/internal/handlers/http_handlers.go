package handlers

import (
	"encoding/json"
	"exchanger/internal/repositories"
	"exchanger/internal/services"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
)

func GetAllExchangeRatesHandle(w http.ResponseWriter, r *http.Request) {
	db, err := new(services.DBService).GetDB()
	if err != nil {
		fmt.Println(err)
		return
	}

	all, err := new(repositories.ExchangeRatesRepository).GetAll(db)

	if err != nil {
		fmt.Println(err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(all)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func GetExchangeRatesByDateHandle(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	db, err := new(services.DBService).GetDB()
	if err != nil {
		fmt.Println(err)
		return
	}

	one, err := new(repositories.ExchangeRatesRepository).GetByDate(db, vars["date"])

	if one != nil {
		w.Header().Set("Content-Type", "application/json")
		err := json.NewEncoder(w).Encode(one)
		if err != nil {
			fmt.Println(err)
			return
		}
	} else {
		w.WriteHeader(http.StatusNotFound)
		_, err := w.Write([]byte("404 page not found"))
		if err != nil {
			fmt.Println(err)
			return
		}
	}
}
