package services

import (
	"exchanger/internal/entities"
	"io"
	"net/http"
	"time"
)

type ExchangeRatesApiService struct{}

func (e *ExchangeRatesApiService) FetchExchangeRates() (*entities.ExchangeRates, error) {

	client := http.Client{}
	resp, err := client.Get("https://api.nbrb.by/exrates/rates?periodicity=0")
	if err != nil {
		return nil, err
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			return
		}
	}(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return nil, err
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var rates entities.ExchangeRates
	rates.Date = time.Now().Format("2006-01-02")
	rates.Rates = string(body)

	return &rates, nil
}
