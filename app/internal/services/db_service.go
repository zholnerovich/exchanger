package services

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"time"
)

type DBService struct{}

func (d *DBService) GetDB() (*sql.DB, error) {
	db, err := sql.Open("mysql", "root:root@tcp(mysql:3306)/exchange_rates")
	if err != nil {
		return nil, err
	}
	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	err = db.Ping()
	if err != nil {
		return nil, err
	}
	return db, nil
}
